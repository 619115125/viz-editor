# viz-editor

> 一个用Graphviz生成流程图的编辑器，使用Ace作为编辑器的容器。可以保存Graphviz代码以及编译结果，也可以把编译结果保存到粘贴板，便于写文档。

## 产品示例

![产品示例](http://oztg5bt5t.bkt.clouddn.com//viz-editor/images/%E7%A4%BA%E4%BE%8B%E5%9B%BE1.png)


## 技术栈

1. [Vue](https://cn.vuejs.org/index.html)  渐进式JavaScript 框架
2. [viz.js](https://github.com/mdaines/viz.js)  用于构建Graphiz。
3. [ace](https://ace.c9.io/)  一个用JavaScriptb编写的可嵌入代码编辑器。
4. [electron](https://electronjs.org/)  使用 JavaScript, HTML 和 CSS 构建跨平台的桌面应用

## Build Setup
``` bash
# 安装依赖
npm install

# 调试模式
npm run dev

# 打包生成
npm run build
```

---

This project was generated with [electron-vue](https://github.com/SimulatedGREG/electron-vue)@[7c4e3e9](https://github.com/SimulatedGREG/electron-vue/tree/7c4e3e90a772bd4c27d2dd4790f61f09bae0fcef) using [vue-cli](https://github.com/vuejs/vue-cli). Documentation about the original structure can be found [here](https://simulatedgreg.gitbooks.io/electron-vue/content/index.html).
